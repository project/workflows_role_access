INTRODUCTION
------------

This module works with the core Workflows module, and allows workflow access to be managed per user role.

This includes creating, editing and deleting workflows, and also access controls for transitions and states.

A use case for this might be situations where many workflows are used by different teams,
but assigning "administer workflows" to all relevant site roles could lead to one team breaking another's workflow.
This way, each team is restricted to only editing their own workflows,
and the admin can easily manage who has access to each workflow just on the edit form.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/workflows_role_access

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/workflows_role_access

REQUIREMENTS
------------

This module requires the following core modules to be enabled:

* Workflows

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

* Configure the user permissions in Administration » People » Permissions:

  - For any role that should have access to a workflow, grant it view, create, edit and/or delete for workflows 
    using the permissions that start with "Access to workflows assigned to the role".
  
  - Generally, you would assign that same role the "States and transitions - create, edit and delete if access to edit workflow" permission. 
    This means if the user can edit the workflow, they can create, edit and delete any states and transitions.
  
  - There is also finer grained control for states and transitions, 
    so you can e.g. allow editing states but not deleting them. 
    Make sure to UNCHECK "States and transitions - create, edit and delete if access to edit workflow" 
    then check the relevant permissions, e.g. "States - create", "Transitions - edit". 

* Edit the workflow:

  - There is now a "Workflow edit access" details fieldset on the workflow edit form.
  
  - Any roles that have permissions configured for role-specific access appear as options, 
    except if that role has the overall "administer workflows" permission.
  
  - Check the roles to apply to that workflow, and the permissions chosen for that role
    will now apply for the workflow.

MAINTAINERS
-----------

Current maintainers: 

* Maxwell Keeble (maxwellkeeble) - https://www.drupal.org/u/maxwellkeeble