<?php

namespace Drupal\workflows_role_access\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
/**
 * Checks access for workflows routes.
 *
 * @package Drupal\workflows_role_access\Controller
 */
class WorkflowsAccessController {

  /**
   * Allow access to collection if the user has any management permissions.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   */
  public function accessCollection(AccountInterface $account) {
    $hasPermission = $account->hasPermission('administer workflows')
      || $account->hasPermission('create workflows assigned to this role')
      || $account->hasPermission('update workflows assigned to this role')
      || $account->hasPermission('delete workflows assigned to this role');
    return AccessResult::allowedIf($hasPermission);
  }
}