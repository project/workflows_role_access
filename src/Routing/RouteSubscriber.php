<?php

namespace Drupal\workflows_role_access\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.workflow.collection')) {
      $route->setRequirements([]);
      $route->setRequirement('_custom_access', '\Drupal\workflows_role_access\Controller\WorkflowsAccessController::accessCollection');
    }

    if($route = $collection->get('entity.workflow.add_form')) {
      $route->setRequirements([]);
      $route->setRequirement('_custom_access', '\Drupal\workflows_role_access\Controller\WorkflowsAccessController::accessCollection');
    }
  }

}
