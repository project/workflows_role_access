<?php

namespace Drupal\workflows_role_access\Access;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\node\Plugin\views\filter\Access;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\workflows\WorkflowAccessControlHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for the Workflow entity.
 *
 * Approach from:
 * https://www.prometsource.com/blog/how-override-entity-access-handlers-drupal
 *
 * @see \Drupal\workflows\Entity\Workflow.
 */
class WorkflowsAccessWorkflowAccessControlHandler extends WorkflowAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\workflows\Entity\Workflow $entity */

    // If they already have access, don't check further:
    $parentResult = parent::checkAccess($entity, $operation, $account);
    if($parentResult->isAllowed()) {
      return $parentResult;
    }

    // Check if access controlled by roles:
    $role_ids = $entity->getThirdPartySetting('workflows_role_access', 'roles');
    if(!$role_ids) {
      return $parentResult;
    }

    // If this is an access check for states or transitions:
    $route_match = \Drupal::routeMatch();
    $workflow_operation = $route_match->getRouteObject()->getRequirement('_workflow_access');
    $type = NULL;
    if($workflow_operation) {
      preg_match('/^(?<operation>add|update|delete)-(?<type>state|transition)$/', $workflow_operation, $matches);
      $operation = $workflow_operation;
    }

    $permissionId = $operation . ' workflows assigned to this role';
    $stateTransitionPermissionId = $type ? 'manage workflows and states and transitions assigned to this role' : '';

    // Check if user has permission (granted by any role):
    $hasPermission = $account->hasPermission($permissionId) || $account->hasPermission($stateTransitionPermissionId);
    if(!$hasPermission) {
      return $parentResult;
    }

    // Check if user has permission from one of these specified roles:
    $account = User::load($account->id());
    foreach($role_ids AS $role_id) {
      $role = Role::load($role_id);
      $hasRole = $account->hasRole($role_id);
      $roleHasPermission = $role->hasPermission($permissionId);
      $roleHasStateTransition = $type && $role->hasPermission($stateTransitionPermissionId);
      if($hasRole && ($roleHasPermission || $roleHasStateTransition)) {
        return AccessResult::allowed();
      }
    }

    return $parentResult;
  }
}